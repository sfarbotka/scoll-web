import sys
import pdb
import argparse

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
)

from scoll.settings import configure as stats_configure
from scoll.tasks.db import task_init_db
from scoll.utils.db import force_rollback
from scoll.utils.datetime import (
    summary_str2date,
    summary_date_format_readable
)
from scoll.models import DBSession
from scoll.tasks.console import task_console
from scoll.tasks.final import task_set_final
from scoll.tasks.summary import task_update_summary
from scoll.tasks.ttype import (
    task_update_traffic_type,
    task_list_unknown
)
from scoll.tasks.importer import task_import
from scoll.tasks.fetch import task_fetch
from scoll.tasks.scache import task_update_stats_cache


def configure(args):
    if args.pdb:
        sys.excepthook = run_debugger

    if args.rollback:
        force_rollback()

    config_uri = args.config
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    stats_configure(settings)


def run_debugger(typ, val, tb):
    print('%r' % val)
    pdb.pm()


def run_task(task, args):
    if task == 'console':
        task_console()
    elif task == 'set-final':
        task_set_final(args.before_date)
    elif task == 'update-summary':
        update_all = args.kind == 'all'
        task_update_summary(update_all)
    elif task == 'update-traffic-type':
        task_update_traffic_type()
    elif task == 'list-unknown':
        task_list_unknown()
    elif task == 'import':
        task_import()
    elif task == 'fetch':
        task_fetch()
    elif task == 'update-scoll-cache':
        update_all = args.kind == 'all'
        task_update_stats_cache(update_all)
    elif task == 'init-db':
        task_init_db()
    else:
        raise NotImplementedError('task %s is not implemented yet' % task)


_final_date_message = """
Incorrect date value: '%%s'.
Date must be in format '%s'""" % (summary_date_format_readable, )


def parse_final_date(sdate):
    try:
        return summary_str2date(sdate)
    except ValueError:
        msg = _final_date_message % (sdate, )
        raise argparse.ArgumentTypeError(msg)


def create_parser():
    parser = argparse.ArgumentParser(description='Execute scoll tasks')
    parser.add_argument('--pdb', help='Run pdb on uncought exception',
                        action='store_true')
    parser.add_argument('--rollback', help='Always rollback database',
                        action='store_true')
    parser.add_argument('config', help='Configuration file')

    ps = parser.add_subparsers(title='tasks', dest='task')
    ps.required = True

    ps.add_parser('console', description='Run interactive console')

    pfinal = ps.add_parser('set-final',
                           description='Set summary final before date')
    pfinal.add_argument('before_date',
                        help='Upper date bound to make summary final',
                        type=parse_final_date
                        )

    ps.add_parser('update-traffic-type',
                  description='Update traffic type')

    ps.add_parser('fetch',
                  description='Fetch data from remote host')

    ps.add_parser('import',
                  description='Import data to DB')

    pss = ps.add_parser('update-summary',
                        description='Update stats summary')
    pss.add_argument('kind', help='Update kind',
                     choices=['dirty', 'all'], nargs='?', default='dirty')

    ps.add_parser('list-unknown',
                  description='List unknown traffic IP addresses')

    psc = ps.add_parser('update-stats-cache',
                        description='Update stats cache')
    psc.add_argument('kind', help='Update kind',
                     choices=['dirty', 'all'], nargs='?', default='dirty')

    ps.add_parser('init-db',
                  description='Initialize DB')

    return parser


def main(argv=sys.argv):
    parser = create_parser()
    args = parser.parse_args(argv[1:])

    configure(args)
    run_task(args.task, args)
