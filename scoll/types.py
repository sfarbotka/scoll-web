from enum import IntEnum


class PeriodType(IntEnum):
    ALL = 0
    YEAR = 1
    MONTH = 2
    DAY = 3
    HOUR = 4

    @classmethod
    def sorted(cls, start_big=True):
        t = tuple(a for a in cls)
        if start_big:
            return t
        return tuple(reversed(t))

    @classmethod
    def superperiod(cls, period):
        assert(isinstance(period, cls))

        t = cls.sorted()
        index = t.index(period)

        return t[index - 1] if index > 0 else None

    @classmethod
    def subperiod(cls, period):
        assert(isinstance(period, cls))

        t = cls.sorted(False)
        index = t.index(period)

        return t[index - 1] if index > 0 else None

    @classmethod
    def cascade(cls, start=HOUR):
        cascade = cls.sorted(False)
        index = cascade.index(start)
        return cascade[index:]

    def super(self):
        return PeriodType.superperiod(self)

    def sub(self):
        return PeriodType.subperiod(self)


class TrafficType(IntEnum):
    GLOBAL = 0
    PEERING = 1
    ISP = 2
    ALIEN = 3
    UNK = 100

    @classmethod
    def summary_possible(cls):
        return cls.GLOBAL, cls.PEERING, cls.ISP


class IPAddrType(IntEnum):
    ROUTER = 0
    ISP = 1
    PEERING = 2
    GLOBAL = 3
