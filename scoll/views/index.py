from pyramid.view import view_config


@view_config(route_name='index', renderer='scoll:templates/views/index.pt')
def my_view(request):
    return {}
