import logging
from datetime import datetime

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPNotFound, HTTPFound

from chameleon.utils import Markup

from scoll.models import DBSession
from scoll.models import Stats, StatsTimestamp

from scoll.query import filter_stats_ts
from scoll.paging import PageItem
from scoll.utils.uriparams import ParamTrafficType, ParamTrafficDir


log = logging.getLogger(__name__)

tsformat = '%Y%m%d%H%M'


@view_defaults(renderer='scoll:templates/views/detailed.pt')
class DetailedViews:
    def __init__(self, request):
        self.request = request

    @staticmethod
    def get_count(start, end, ttype):
        q = DBSession.query(Stats).join(StatsTimestamp)

        if ttype.val is not None:
            q = q.filter(Stats.ttype == ttype.val)
        q = filter_stats_ts(q, start, end)

        return q.count()

    @staticmethod
    def get_data(start, end, ttype, page, limit, tdir):
        q = DBSession.query(Stats).join(StatsTimestamp)

        if ttype.val is  not None:
            q = q.filter(Stats.ttype == ttype.val)
        if not tdir.all:
            q = q.filter(Stats.is_inbound == tdir.inbound)
        q = filter_stats_ts(q, start, end)
        q = q.offset(page * limit).limit(limit)

        return q.all()

    @property
    def parse_params(self):
        pttype = self.request.params.get('ttype',
                                         ParamTrafficType.default()).upper()
        pstart = self.request.params.get('start', None)
        pend = self.request.params.get('end', None)
        ppage = self.request.params.get('page', '1')
        ptdir = self.request.params.get('tdir',
                                        ParamTrafficDir.default()).upper()

        def strptime(dt):
            return datetime.strptime(dt, tsformat)

        try:
            tdir = ParamTrafficDir(ptdir)
            ttype = ParamTrafficType(pttype)
            start = strptime(pstart) if pstart else None
            end = strptime(pend) if pend else None
        except ValueError:
            raise HTTPNotFound

        try:
            page = int(ppage) - 1
        except ValueError:
            raise HTTPNotFound

        if page < 0:
            raise HTTPNotFound

        return ttype, start, end, page, tdir

    @staticmethod
    def url_query(start, end, ttype, page, tdir):
        ret = {}
        if start is not None:
            ret['start'] = start.strftime(tsformat)
        if end is not None:
            ret['end'] = end.strftime(tsformat)

        ret['ttype'] = str(ttype)
        ret['page'] = str(page + 1)
        ret['tdir'] = str(tdir)

        return ret

    def generate_pages(self, start, end, ttype, page, pcount, tdir):
        ret = []

        if page > 0:
            uq = self.url_query(start, end, ttype, page - 1, tdir)
            p = PageItem(Markup('&laquo;'),
                         self.request.current_route_url(_query=uq))
            ret.append(p)

        if page > 2:
            uq = self.url_query(start, end, ttype, 0, tdir)
            p = PageItem('1', self.request.current_route_url(_query=uq))
            ret.append(p)

        if page > 3:
            p = PageItem('...', None, enabled=False)
            ret.append(p)

        for i in range(max(0, page - 2), page):
            uq = self.url_query(start, end, ttype, i, tdir)
            p = PageItem('%s' % (i + 1),
                         self.request.current_route_url(_query=uq))
            ret.append(p)

        p = PageItem('%s' % (page + 1), None, active=True)
        ret.append(p)

        for i in range(page + 1, min(page + 3, pcount)):
            uq = self.url_query(start, end, ttype, i, tdir)
            p = PageItem('%s' % (i + 1),
                         self.request.current_route_url(_query=uq))
            ret.append(p)

        if page < pcount - 4:
            p = PageItem('...', None, enabled=False)
            ret.append(p)

        if page < pcount - 3:
            uq = self.url_query(start, end, ttype, pcount - 1, tdir)
            p = PageItem('%s' % (pcount, ),
                         self.request.current_route_url(_query=uq))
            ret.append(p)

        if page < pcount - 1:
            uq = self.url_query(start, end, ttype, page + 1, tdir)
            p = PageItem(Markup('&raquo;'),
                         self.request.current_route_url(_query=uq))
            ret.append(p)

        return ret

    @view_config(route_name='detailed')
    def detailed(self):
        ttype, start, end, page, tdir = self.parse_params

        count = self.get_count(start, end, ttype)
        if count:
            limit = 100
            pcount = int((count + limit - 1) / limit)
            if page >= pcount:
                urlq = self.url_query(start, end, ttype, pcount - 1, tdir)
                url = self.request.current_route_url(_query=urlq)
                raise HTTPFound(location=url)

            q = self.get_data(start, end, ttype, page, limit, tdir)
            p = self.generate_pages(start, end, ttype, page, pcount, tdir)
            istart = page * limit
        else:
            q = None
            p = None
            istart = None

        return dict(query=q,
                    pages=p,
                    tsformat='%Y-%m-%d %H:%M',
                    istart=istart
                    )
