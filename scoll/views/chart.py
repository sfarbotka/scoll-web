from collections import defaultdict
import logging
from datetime import datetime, timedelta
from pyramid.httpexceptions import HTTPNotFound

from pyramid.view import view_config
from sqlalchemy import func


from scoll.query import filter_stats_ts
from scoll.types import TrafficType
from scoll.utils.datetime import datetime_range, timestamp_date2str, \
    timestamp_str2date
from scoll.utils.uriparams import ParamTrafficType, ParamTrafficDir

from scoll.models import (
    DBSession,
    StatsTimestamp,
    Stats,
    StatsCache,
)


log = logging.getLogger(__name__)


class ChartContainer:
    def __init__(self):
        self.title = '<title>'
        self.subtitle = '<subtitle>'


_ttypes_to_query = (TrafficType.GLOBAL,
                    TrafficType.PEERING,
                    TrafficType.ISP)


class ChartUnit:
    def __init__(self):
        self._vals = dict((e, 0) for e in _ttypes_to_query)

    def add(self, ttype, nbytes):
        self._vals[ttype] += nbytes

    def get(self, ttype):
        return int(self._vals[ttype])


class ChartView:
    def __init__(self, request):
        self.request = request

    @property
    def json_params(self):
        try:
            pttype = self.request.params.get('ttype',
                                             ParamTrafficType.default()).upper()
            pstart = self.request.params.get('start')
            pend = self.request.params.get('end', None)
            if not pend:
                pend = None
            pnintervals = self.request.params.get('nintervals', '30')
            ptdir = self.request.params.get('tdir',
                                            ParamTrafficDir.default()).upper()
        except KeyError:
            raise HTTPNotFound

        def strptime(dt):
            return timestamp_str2date(dt)

        try:
            tdir = ParamTrafficDir(ptdir)
            ttype = ParamTrafficType(pttype).val
            start = strptime(pstart)
            end = strptime(pend) if pend else datetime.now()
        except ValueError:
            raise HTTPNotFound

        try:
            nintervals = int(pnintervals)
        except ValueError:
            raise HTTPNotFound

        if nintervals <= 0 or nintervals > 100:
            raise HTTPNotFound

        return ttype, start, end, nintervals, tdir

    @staticmethod
    def create_query(ttype):
        q = DBSession.query(
            func.sum(StatsCache.in_bytes).label('in_bytes'),
            func.sum(StatsCache.out_bytes).label('out_bytes'),
            func.sum(StatsCache.unk_bytes).label('unk_bytes'),
            StatsCache.ttype).join(StatsTimestamp).group_by(StatsCache.ttype)

        if ttype is not None:
            q = q.filter(StatsCache.ttype == ttype)
        return q

    @staticmethod
    def get_nbytes(crec, tdir):
        if tdir.all:
            return crec.in_bytes + crec.out_bytes + crec.unk_bytes
        if tdir.inbound is True:
            return crec.in_bytes
        if tdir.inbound is False:
            return crec.out_bytes
        if tdir.inbound is None:
            return crec.unk_bytes

    @view_config(route_name='chart/json', renderer='json')
    def json_data(self):
        ttype, start, end, nintervals, tdir = self.json_params

        step = (end - start) / nintervals

        vals = defaultdict(ChartUnit)
        dates = []

        q = self.create_query(ttype)

        for date in datetime_range(start, end, step):
            dates.append(date)
            nextdate = date + step

            for c in filter_stats_ts(q, date, nextdate):
                if c.ttype in _ttypes_to_query:
                    nbytes = self.get_nbytes(c, tdir)
                    vals[date].add(c.ttype, nbytes)

        series = []

        ttypes = _ttypes_to_query if ttype is None else (ttype, )
        for ttype in ttypes:
            data = []
            for date in dates:
                data.append(vals[date].get(ttype))

            d = {'name': ttype.name,
                 'data': data}
            series.append(d)

        return {'dates': tuple(timestamp_date2str(a) for a in dates),
                'series': series}

    @view_config(route_name='chart/',
                 renderer='scoll:templates/views/chart_last.pt')
    def chart(self):
        now = datetime.now()
        last24hours_q = {
            'start': timestamp_date2str(now - timedelta(hours=24))
        }
        last24hours = self.request.route_url('chart/json',
                                             _query=last24hours_q)

        last30days_q = {
            'start': timestamp_date2str(now - timedelta(days=30))
        }
        last30days = self.request.route_url('chart/json',
                                            _query=last30days_q)

        return {
            'url_last24hours': last24hours,
            'url_last30days': last30days
        }

    @view_config(route_name='chart/custom',
                 renderer='scoll:templates/views/chart_custom.pt')
    def chart_custom(self):
        now = datetime.now()
        default_sdate = timestamp_date2str(now - timedelta(hours=24))

        json_url = self.request.route_url('chart/json')

        return {
            'json_url': json_url,
            'ttypes': ParamTrafficType.all_values(),
            'tdirs': ParamTrafficDir.all_values(),
            'default_sdate': default_sdate,
            'default_nintervals': 30
        }
