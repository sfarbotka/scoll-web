from datetime import datetime
import logging

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPNotFound

from sqlalchemy.orm.exc import NoResultFound

from scoll.models import (
    DBSession,
    StatsSummary,
    )

from scoll.types import PeriodType, TrafficType
from scoll.util import size_bytes_to_string


log = logging.getLogger(__name__)


@view_defaults(renderer='scoll:templates/views/summary.pt')
class SummaryViews:
    def __init__(self, request):
        self.request = request

    def get_summary(self, sdate=None, period=PeriodType.ALL):
        # filter by timestamps
        def create_query(ttype):
            q = DBSession.query(StatsSummary).filter(
                StatsSummary.ttype == ttype,
                StatsSummary.period == period)

            if period != PeriodType.ALL:
                q = q.filter(StatsSummary.sdate == sdate)
            return q

        qg, qp, qi = map(create_query, (TrafficType.GLOBAL,
                                        TrafficType.PEERING,
                                        TrafficType.ISP))

        # get values
        def tonums(q):
            try:
                v = q.one()
            except NoResultFound:
                return -1, -1
            return v.in_bytes, v.out_bytes

        def calc_n(name, io_tuple):
            ki, ko, kt = map(lambda e: '_'.join((name, e)), ('in',
                                                             'out',
                                                             'total'))
            d = {}
            ni, no = io_tuple
            d[ki], d[ko], d[kt] = ni, no, (ni + no)
            return d

        n = {}
        n.update(calc_n('global', tonums(qg)))
        n.update(calc_n('peering', tonums(qp)))
        n.update(calc_n('isp', tonums(qi)))

        io_vals = (sum(n['_'.join((s, e))] for s in ('global',
                                                     'peering',
                                                     'isp')) for e in ('in',
                                                                       'out'))

        n.update(calc_n('all', io_vals))

        summary = {k: size_bytes_to_string(v) for k, v in n.items()}

        return summary

    def get_route_date(self):
        try:
            sy = self.request.matchdict.get('year')
            sm = self.request.matchdict.get('month', '1')
            sd = self.request.matchdict.get('day', '1')
            sh = self.request.matchdict.get('hour', '0')

            date = datetime(int(sy), int(sm), int(sd), int(sh))
        except ValueError:
            raise HTTPNotFound

        return date

    @view_config(route_name='summary-all')
    def all_period(self):

        summary = self.get_summary()

        return dict(summary=summary,
                    period_name='Summary for all period',
                    pages=None
                    )

    @view_config(route_name='summary-year')
    def year(self):
        sdate = self.get_route_date()

        summary = self.get_summary(sdate, PeriodType.YEAR)
        return dict(summary=summary,
                    period_name=sdate.strftime('%Y'),
                    pages=None
                    )

    @view_config(route_name='summary-month')
    def month(self):
        sdate = self.get_route_date()

        summary = self.get_summary(sdate, PeriodType.MONTH)
        return dict(summary=summary,
                    period_name=sdate.strftime('%Y-%m'),
                    pages=None
                    )

    @view_config(route_name='summary-day')
    def day(self):
        sdate = self.get_route_date()

        summary = self.get_summary(sdate, PeriodType.DAY)
        return dict(summary=summary,
                    period_name=sdate.strftime('%Y-%m-%d'),
                    pages=None
                    )

    @view_config(route_name='summary-hour')
    def hour(self):
        sdate = self.get_route_date()

        summary = self.get_summary(sdate, PeriodType.HOUR)
        return dict(summary=summary,
                    period_name=sdate.strftime('%Y-%m-%d-%H'),
                    pages=None
                    )
