from pyramid.events import (
    subscriber,
    BeforeRender)

from pyramid.renderers import get_renderer


@subscriber(BeforeRender)
def add_base_template(event):
    base = get_renderer('scoll:templates/base.pt').implementation()
    event.update({'macros_base': base})
    event.update({'nav_active': event['request'].matched_route.name})
