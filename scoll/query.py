from scoll.models import (
    DBSession,
    StatsTimestamp,
    StatsSummary
)
from scoll.utils.datetime import increment_date, date_for_period
from scoll.types import PeriodType


def filter_stats_ts(q, start, end):
    if start is not None:
        q = q.filter(StatsTimestamp.ts >= start)
    if end is not None:
        q = q.filter(StatsTimestamp.ts < end)

    return q


def filter_summary_subperiod(q, period, period_date):
    q = q.filter(StatsSummary.period == period.sub())

    if period is PeriodType.ALL:
        return q

    sdate = date_for_period(period_date, period)
    edate = increment_date(period_date, period)

    q = q.filter(StatsSummary.sdate >= sdate,
                 StatsSummary.sdate < edate)

    return q


def filter_stats_period(q, period, period_date):
    sdate = date_for_period(period_date, period)
    edate = increment_date(period_date, period)

    return filter_stats_ts(q, sdate, edate)


def query_stats_ts_bounds():
    # Query first date
    r = DBSession.query(StatsTimestamp.ts).order_by(
        StatsTimestamp.ts_id.asc()).first()
    if r is None:
        return (None, None)
    sdate = r.ts

    # Query last date
    r = DBSession.query(StatsTimestamp.ts).order_by(
        StatsTimestamp.ts_id.desc()).first()
    edate = r.ts

    return (sdate, edate)
