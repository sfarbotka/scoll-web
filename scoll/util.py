_pfloat = '%.1f %s'
_pint = '%d %s'


def size_bytes_to_string(num):
    if num is None or num < 0:
        return '?? bytes'

    val = float(num)

    for s, pattern in [('bytes', _pint),
                       ('KB', _pfloat),
                       ('MB', _pfloat),
                       ('GB', _pfloat)]:
        if val < 1024.0:
            return pattern % (val, s)
        val /= 1024.0

    return _pfloat % (val, 'TB')
