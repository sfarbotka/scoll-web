from sqlalchemy import (
    Column,
    Integer,
    BigInteger,
    SmallInteger,
    DateTime,
    ForeignKey,
    String,
    Boolean,
    CheckConstraint,
    )

from sqlalchemy.types import TypeDecorator

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    backref
    )

from zope.sqlalchemy import ZopeTransactionExtension

from scoll.types import (
    PeriodType,
    TrafficType,
)


DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


def period_check_constraint_sql(field_name):
    vals = tuple(map(lambda x: x.value, PeriodType))
    return '%s IN %s' % (field_name, vals)


class PeriodSQLType(TypeDecorator):
    impl = SmallInteger

    def process_bind_param(self, value, dialect):
        return value.value

    def process_result_value(self, value, dialect):
        return PeriodType(value)


def ttype_stats_check_constraint_sql(field_name):
    vals = tuple(x.value for x in TrafficType)
    return '%s IN %s' % (field_name, vals)


def ttype_summary_check_constraint_sql(field_name):
    vals = tuple(x.value for x in TrafficType.summary_possible())
    return '%s IN %s' % (field_name, vals)


class TrafficSQLType(TypeDecorator):
    impl = SmallInteger

    def process_bind_param(self, value, dialect):
        return value.value

    def process_result_value(self, value, dialect):
        return TrafficType(value)


class StatsTimestamp(Base):
    __tablename__ = 'stats_ts'
    ts_id = Column(Integer, primary_key=True)
    ts = Column(DateTime, nullable=False, unique=True, index=True)

    stats = relationship('Stats',
                         backref=backref('timestamp',
                                         order_by=ts_id,
                                         innerjoin=True),
                         cascade='all,delete-orphan')
    scache = relationship('StatsCache',
                          backref=backref('timestamp',
                                          order_by=ts_id,
                                          innerjoin=True),
                          cascade='all,delete-orphan')


class Stats(Base):
    __tablename__ = 'stats'

    _ck_ttype = CheckConstraint(ttype_stats_check_constraint_sql('ttype'))

    stats_id = Column(Integer, primary_key=True)
    ts_id = Column(Integer, ForeignKey('stats_ts.ts_id',
                                       onupdate='CASCADE',
                                       ondelete='CASCADE'), nullable=False)
    src_ip = Column(String(50), nullable=False)
    dst_ip = Column(String(50), nullable=False)
    src_port = Column(Integer)
    dst_port = Column(Integer)
    num_packets = Column(Integer, nullable=False)
    num_bytes = Column(Integer, nullable=False)
    is_inbound = Column(Boolean)
    ttype = Column(TrafficSQLType, _ck_ttype, nullable=False, index=True)


class StatsCache(Base):
    __tablename__ = 'stats_cache'

    _ck_ttype = CheckConstraint(ttype_stats_check_constraint_sql('ttype'))

    cache_id = Column(Integer, primary_key=True)
    ts_id = Column(Integer, ForeignKey('stats_ts.ts_id',
                                       onupdate='CASCADE',
                                       ondelete='CASCADE'), nullable=False)
    in_bytes = Column(BigInteger, nullable=False)
    out_bytes = Column(BigInteger, nullable=False)
    unk_bytes = Column(BigInteger, nullable=False)
    accurate = Column(Boolean, nullable=False)
    ttype = Column(TrafficSQLType, _ck_ttype, nullable=False)


class StatsSummary(Base):
    __tablename__ = 'stats_summary'

    _ck_period = CheckConstraint(period_check_constraint_sql('period'))
    _ck_ttype = CheckConstraint(ttype_summary_check_constraint_sql('ttype'))

    summary_id = Column(Integer, primary_key=True, autoincrement=True)
    sdate = Column(DateTime, nullable=False)
    period = Column(PeriodSQLType, _ck_period, nullable=False)
    ttype = Column(TrafficSQLType, _ck_ttype, nullable=False)
    in_bytes = Column(BigInteger, nullable=False)
    out_bytes = Column(BigInteger, nullable=False)
    accurate = Column(Boolean, nullable=False)
    final = Column(Boolean, nullable=False)


__all__ = [
    DBSession,
    Base,
    PeriodType,
    Stats,
    StatsTimestamp,
    StatsSummary,
]
