from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from scoll.models import (
    DBSession,
    Base,
    )


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('index', '/')

    # Summary routes
    config.add_route('summary-all', '/summary/')
    config.add_route('summary-year', '/summary/{year}')
    config.add_route('summary-month', '/summary/{year}/{month}')
    config.add_route('summary-day', '/summary/{year}/{month}/{day}')
    config.add_route('summary-hour', '/summary/{year}/{month}/{day}/{hour}')

    # Detailed route
    config.add_route('detailed', '/detailed.html')

    # Chart routes
    config.add_route('chart/', '/chart/')
    config.add_route('chart/custom', '/chart/custom.html')
    config.add_route('chart/json', '/chart/data.json')

    config.scan('scoll.views')

    return config.make_wsgi_app()
