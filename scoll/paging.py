

class PageItem:
    def __init__(self, text, url, enabled=True, active=False):
        self.text = text
        self.url = url
        self.enabled = enabled
        self.active = active

    def get_class_attr(self):
        a = []
        if not self.enabled:
            a.append('disabled')
        if self.active:
            a.append('active')

        return ' '.join(a) if len(a) else None
