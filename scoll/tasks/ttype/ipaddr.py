from ipaddress import ip_address, ip_network

from scoll.settings import settings
from scoll.tasks.ttype.peering import check_peering
from scoll.types import IPAddrType


_local_networks = None
_loaded = False


def _iter_ips(f):
    for line in f:
        l = line.strip().split('#', 1)[0].strip()
        if not len(l):
            continue

        yield l


def _load_local_networks():
    global _loaded
    global _local_networks

    _local_networks = []

    fname = settings('tasks.ttype.local-networks-file')
    with open(fname, 'r') as f:
        _local_networks.extend(ip_network(ip) for ip in _iter_ips(f))

    _loaded = True


def _is_local(ip):
    for net in _local_networks:
        if ip in net:
            return True

    return False


def classify_ipaddr(ipstr):
    if not _loaded:
        _load_local_networks()

    ip = ip_address(ipstr)

    if _is_local(ip):
        return IPAddrType.ROUTER
    if ip.is_private or ip.is_multicast:
        return IPAddrType.ISP
    if check_peering(ip):
        return IPAddrType.PEERING

    return IPAddrType.GLOBAL
