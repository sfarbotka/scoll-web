import logging

from scoll.tasks.summary import update_all_summary
from scoll.tasks.ttype.ipaddr import classify_ipaddr
from scoll.utils.db import transact

from scoll.types import (
    TrafficType,
    IPAddrType,
)

from scoll.models import (
    DBSession,
    Stats,
)


log = logging.getLogger(__name__)


_ttype_tup = ((IPAddrType.GLOBAL, IPAddrType.ROUTER, TrafficType.GLOBAL),
              (IPAddrType.GLOBAL, IPAddrType.ISP, TrafficType.UNK),
              (IPAddrType.GLOBAL, IPAddrType.PEERING, TrafficType.UNK),
              (IPAddrType.GLOBAL, IPAddrType.GLOBAL, TrafficType.UNK),
              (IPAddrType.PEERING, IPAddrType.ROUTER, TrafficType.PEERING),
              (IPAddrType.PEERING, IPAddrType.ISP, TrafficType.UNK),
              (IPAddrType.PEERING, IPAddrType.PEERING, TrafficType.UNK),
              (IPAddrType.ISP, IPAddrType.ROUTER, TrafficType.ISP),
              (IPAddrType.ISP, IPAddrType.ISP, TrafficType.ALIEN),
              (IPAddrType.ROUTER, IPAddrType.ROUTER, TrafficType.UNK),
              )

_ttype_map = dict((frozenset((k1, k2)), v) for k1, k2, v in _ttype_tup)


def update_ttype(rec, src_ip, dst_ip):
    tsrc = classify_ipaddr(src_ip)
    tdst = classify_ipaddr(dst_ip)

    rec.ttype = _ttype_map[frozenset((tsrc, tdst))]
    lsrc = (tsrc is IPAddrType.ROUTER)
    ldst = (tdst is IPAddrType.ROUTER)

    if lsrc != ldst:
        # one is True, another one is False
        rec.is_inbound = ldst
    else:
        rec.is_inbound = None


def _iter_unknown_pair():
    q = DBSession.query(Stats).filter(Stats.ttype == TrafficType.UNK)

    for s in q.yield_per(100):
        yield (s.src_ip, s.dst_ip)


@transact
def task_list_unknown():
    for i, (src, dst) in enumerate(_iter_unknown_pair()):
        print('%s -> %s' % (src, dst))
        if i > 10:
            break


@transact
def task_update_traffic_type():
    log.info('Updating traffic type for all records...')

    for rec in DBSession.query(Stats).yield_per(100):
        update_ttype(rec, rec.src_ip, rec.dst_ip)
        DBSession.add(rec)

    update_all_summary()
