import re

import http.client
import urllib.parse

import GeoIP

from scoll.settings import settings


gi = None


def check_peering(ip, legacy=False):
    if legacy:
        return _legacy_request(ip)
    else:
        return _new_request(ip)


def _new_request(ip):
    global gi
    if gi is None:
        gi = GeoIP.open(settings('tasks.ttype.geoip-database'),
                        GeoIP.GEOIP_MEMORY_CACHE)

    return (gi.country_code_by_addr(str(ip)) == 'BY')


def _legacy_request(ip):
    params = urllib.parse.urlencode({'addres': str(ip)})
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}
    conn = http.client.HTTPConnection('help.telecom.by')
    conn.request('POST', '/faq/is_peering/', params, headers)
    response = conn.getresponse()
    if response.status != 200:
        return None
    data = response.read().decode('cp1251')
    conn.close()

    return _find_result(data)


_regex_found = re.compile(r'<div><font color=\"green\">[^<>]+</font></div>')
_regex_notfound = re.compile(r'<div><font color=\"red\">[^<>]+</font></div>')


def _find_result(data):
    f = _regex_found.search(data) is not None
    nf = _regex_notfound.search(data) is not None

    return f if f != nf else None
