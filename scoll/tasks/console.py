import sys
from code import interact

from scoll.utils.db import (
    transact,
    force_rollback,
    RollbackException
)


_banner = """\
scoll interactive console
type 'abort()' to rollback transaction and exit
type 'exit()' or 'done()' to commit and exit
type 'exit(<exitcode>)' to commit if <exitcode> = 0, else rollback, then exit
type <Ctrl-D> (EOF) to rollback and exit
"""


def _abort():
    sys.exit(RollbackException())


def _done():
    sys.exit()


def _get_locals(module):
    a = module.__all__
    d = filter(lambda x: x[1] in a, module.__dict__.items())

    return d


@transact
def _run_console():

    import scoll.models

    loc = {}
    loc.update(_get_locals(scoll.models))
    loc['abort'] = _abort
    loc['done'] = _done
    loc['force_rollback'] = force_rollback

    try:
        interact(banner=_banner, local=loc)
        _abort()
    except SystemExit as e:
        ret = e.code
        if isinstance(ret, BaseException):
            raise ret
        if ret is not None and ret != 0:
            raise


def task_console():
    _run_console()
