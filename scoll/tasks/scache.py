import logging

from sqlalchemy import (
    func,
    false,
)

from scoll.models import (
    DBSession,
    StatsCache,
    StatsTimestamp,
    Stats,
)

from scoll.types import (
    TrafficType,
)
from scoll.utils.datetime import timestamp_date2str

from scoll.utils.db import transact
from scoll.utils.traffic import TrafficBytes


log = logging.getLogger(__name__)


def get_scache_record(ts_id, ttype):
    assert(isinstance(ttype, TrafficType))

    q = DBSession.query(StatsCache).filter(StatsCache.ts_id == ts_id,
                                           StatsCache.ttype == ttype)
    ret = q.first()
    if ret is None:
        log.debug('Creating StatsCache for ts_id=%s, ttype=%s' % (
            ts_id, ttype.name))

        ret = StatsCache(ts_id=ts_id,
                         ttype=ttype,
                         in_bytes=0,
                         out_bytes=0,
                         unk_bytes=0,
                         accurate=False)

    return ret


def scache_set_dirty(trec):
    log.debug('Set stats cache dirty for %s' % (
        timestamp_date2str(trec.ts), ))

    for ttype in TrafficType:
        crec = get_scache_record(trec.ts_id, ttype)

        crec.accurate = False
        DBSession.add(crec)


def _calc_traffic_for_ts_and_ttype(ts, ttype):
    ret = TrafficBytes()

    q = DBSession.query(func.sum(Stats.num_bytes).label('num_bytes'),
                        Stats.is_inbound).\
        join(StatsTimestamp).\
        filter(Stats.timestamp == ts, Stats.ttype == ttype).\
        group_by(Stats.is_inbound)

    for r in q:
        if r.is_inbound is True:
            ret.ib = r.num_bytes
        elif r.is_inbound is False:
            ret.ob = r.num_bytes
        else:
            ret.ub = r.num_bytes

    return ret


def scache_update_dirty():
    log.info('Updating dirty stats cache...')

    q = DBSession.query(StatsCache).filter(StatsCache.accurate == false())

    for crec in q.yield_per(100):
        iob = _calc_traffic_for_ts_and_ttype(crec.timestamp, crec.ttype)

        crec.in_bytes = iob.ib
        crec.out_bytes = iob.ob
        crec.unk_bytes = iob.ub
        crec.accurate = True
        DBSession.add(crec)


def _calc_traffic_for_ts(ts):
    ret = dict()
    for ttype in TrafficType:
        ret[ttype] = TrafficBytes()

    q = DBSession.query(func.sum(Stats.num_bytes).label('num_bytes'),
                        Stats.is_inbound, Stats.ttype).\
        join(StatsTimestamp).\
        filter(Stats.timestamp == ts).\
        group_by(Stats.is_inbound, Stats.ttype)

    for r in q:
        iob = ret[r.ttype]

        if r.is_inbound is True:
            iob.ib = r.num_bytes
        elif r.is_inbound is False:
            iob.ob = r.num_bytes
        else:
            iob.ub = r.num_bytes

    return ret


def scache_update_all():
    log.info('Updating all stats cache...')
    q = DBSession.query(StatsTimestamp)

    for ts in q:
        s = _calc_traffic_for_ts(ts)
        for ttype, iob in s.items():
            crec = get_scache_record(ts.ts_id, ttype)

            crec.in_bytes = iob.ib
            crec.out_bytes = iob.ob
            crec.unk_bytes = iob.ub
            crec.accurate = True
            DBSession.add(crec)


@transact
def task_update_stats_cache(update_all):
    if update_all:
        scache_update_all()
    else:
        scache_update_dirty()
