import logging
from collections import defaultdict

from sqlalchemy import (
    func,
    false,
)

from sqlalchemy.orm import joinedload

from scoll.utils.db import transact
from scoll.types import (
    PeriodType,
    TrafficType,
)

from scoll.utils.datetime import (
    date_for_period,
    summary_date2str,
)

from scoll.utils.traffic import TrafficBytes

from scoll.models import (
    DBSession,
    Stats,
    StatsSummary,
    StatsTimestamp,
    StatsCache,
)

from scoll.query import (
    filter_stats_period,
    filter_summary_subperiod,
)

from scoll.tasks.scache import scache_update_dirty


log = logging.getLogger(__name__)


def get_summary_record(date, period, ttype, no_final=True):
    assert(isinstance(ttype, TrafficType))

    if period == PeriodType.ALL:
        date = date_for_period(date, period)

    q = DBSession.query(StatsSummary).filter(StatsSummary.period == period,
                                             StatsSummary.sdate == date,
                                             StatsSummary.ttype == ttype)
    ret = q.first()
    if ret is None:
        log.debug('Creating StatsSummary for %s, date=%s, ttype=%s' % (
            period.name, summary_date2str(date), ttype.name))

        ret = StatsSummary(period=period,
                           sdate=date,
                           accurate=False,
                           final=False,
                           ttype=ttype,
                           in_bytes=0,
                           out_bytes=0)
    elif no_final and ret.final is True:
        log.debug('Skipping final summary record')
        return None

    return ret


class DirtyDatesSet:
    def __init__(self, sort_iter=True):
        self._dirty = set()
        self._sort_iter = sort_iter

    def add(self, period, date):
        ddate = date_for_period(date, period)
        t = (period, ddate)
        self._dirty.add(t)

    def __iter__(self):
        a = sorted(self._dirty) if self._sort_iter else self._dirty
        return iter(a)


def update_summary_dirty(dset):
    for period, ddate in dset:
        log.debug('Set dirty summary: %s %s' %
                  (period, summary_date2str(ddate)))

        for ttype in TrafficType.summary_possible():
            srec = get_summary_record(ddate, period, ttype)
            if srec is None:
                continue
            srec.accurate = False
            DBSession.add(srec)


def _calc_summary_for_hour(date, ttype, use_cache):
    ret = TrafficBytes()

    if use_cache:
        # Query StatsCache table
        q = DBSession.query(
            func.sum(StatsCache.in_bytes).label('in_bytes'),
            func.sum(StatsCache.out_bytes).label('out_bytes'),
            func.sum(StatsCache.unk_bytes).label('unk_bytes')).\
            join(StatsTimestamp).\
            filter(StatsCache.ttype == ttype)
        q = filter_stats_period(q, PeriodType.HOUR, date)

        crec = q.first()
        if crec is not None:
            ret.ib = crec.in_bytes
            ret.ob = crec.out_bytes
            ret.ub = crec.unk_bytes
    else:
        # Query Stats table
        q = DBSession.query(func.sum(Stats.num_bytes).label('num_bytes'),
                            Stats.is_inbound).join(StatsTimestamp).\
            filter(Stats.ttype == ttype)
        q = filter_stats_period(q, PeriodType.HOUR, date)

        for r in q.group_by(Stats.is_inbound):
            if r.is_inbound is True:
                ret.ib = r.num_bytes
            elif r.is_inbound is False:
                ret.ob = r.num_bytes
            else:
                ret.ub = r.num_bytes

    return ret


def _calc_summary_for_period(period, date, ttype):
    if period is PeriodType.HOUR:
        return _calc_summary_for_hour(date, ttype, True)
    else:
        # Query StatsSummary
        q = DBSession.query(
            func.sum(StatsSummary.in_bytes).label('isum'),
            func.sum(StatsSummary.out_bytes).label('osum')).filter(
                StatsSummary.ttype == ttype)

        r = filter_summary_subperiod(q, period, date).first()
        # Update summary
        ret = TrafficBytes()
        if r is not None:
            ret.ib = r.isum
            ret.ob = r.osum

        return ret


def _update_summary_for_period(period):
    log.debug('Updating summary for period: %s' % period.name)

    dset = DirtyDatesSet()
    superperiod = period.super()

    # Query dirty records
    qupdate = DBSession.query(StatsSummary).filter(
        StatsSummary.period == period,
        StatsSummary.final == false(),
        StatsSummary.accurate == false())

    for sdirty in qupdate:
        curdate = sdirty.sdate
        log.debug('Updating StatsSummary for %s, date=%s, ttype=%s' % (
            period.name, summary_date2str(curdate), sdirty.ttype.name))

        if superperiod is not None:
            dset.add(superperiod, curdate)

        iob = _calc_summary_for_period(period, curdate, sdirty.ttype)

        sdirty.in_bytes = iob.ib
        sdirty.out_bytes = iob.ob
        sdirty.accurate = True

        DBSession.add(sdirty)

    update_summary_dirty(dset)


def cascaded_summary_update(start_period):
    scache_update_dirty()
    if start_period == PeriodType.HOUR:
        scache_update_dirty()

    cascade = PeriodType.cascade(start_period)
    log.debug('Summary update cascade: %s' % (
        ', '.join(p.name for p in cascade)))

    for period in cascade:
        _update_summary_for_period(period)


def update_all_summary():
    scache_update_dirty()
    log.info('Updating summary...')

    nbytes = defaultdict(TrafficBytes)
    dset = DirtyDatesSet()

    period = PeriodType.HOUR
    superperiod = period.super()

    q = DBSession.query(StatsCache).options(joinedload('timestamp'))

    log.debug('Querying traffic...')
    for r in q.yield_per(100):
        if r.ttype not in TrafficType.summary_possible():
            continue

        ts = r.timestamp.ts
        sdate = date_for_period(ts, period)
        dset.add(superperiod, ts)
        k = (sdate, r.ttype)
        nbytes[k].ib += r.in_bytes
        nbytes[k].ob += r.out_bytes
        nbytes[k].ub += r.unk_bytes

    for (sdate, ttype), numbytes in nbytes.items():
        log.debug('Updating StatsSummary for %s, date=%s, ttype=%s' % (
            period.name, summary_date2str(sdate), ttype.name))
        srec = get_summary_record(sdate, period, ttype)
        if srec is None:
            continue
        srec.in_bytes = numbytes.ib
        srec.out_bytes = numbytes.ob
        srec.accurate = True
        DBSession.add(srec)

    update_summary_dirty(dset)
    cascaded_summary_update(period)


@transact
def task_update_summary(update_all):
    if update_all:
        update_all_summary()
    else:
        cascaded_summary_update(PeriodType.HOUR)
