import logging

from sqlalchemy import (
    false,
    true
)

from scoll.types import PeriodType, TrafficType
from scoll.utils.db import transact
from scoll.settings import settings
from scoll.utils.datetime import (
    summary_date2str,
    summary_str2date,
    date_for_period,
)

from scoll.tasks.summary import (
    cascaded_summary_update,
    get_summary_record,
    update_all_summary,
    update_summary_dirty,
    DirtyDatesSet,
)

from scoll.models import (
    DBSession,
    StatsSummary,
    StatsTimestamp,
)


log = logging.getLogger(__name__)


def _set_final_for_period(period, edate):
    edate = date_for_period(edate, period)
    log.debug('Making summary final for period: %s before date %s' %
              (period.name, summary_date2str(edate)))

    # Query dirty records
    q = DBSession.query(StatsSummary).filter(
        StatsSummary.period == period,
        StatsSummary.final == false(),
        StatsSummary.sdate < edate)

    for s in q.order_by(StatsSummary.sdate):
        log.debug('Set final for %s, date=%s, ttype=%s' %
                  (period.name, summary_date2str(s.sdate), s.ttype.name))

        s.final = True
        DBSession.add(s)


def _cascaded_set_final(edate):
    cascade = PeriodType.cascade()
    for period in cascade:
        _set_final_for_period(period, edate)


class SummaryBackup:
    _period_map = {
        PeriodType.ALL: 'A',
        PeriodType.YEAR: 'Y',
        PeriodType.MONTH: 'M',
        PeriodType.DAY: 'D',
        PeriodType.HOUR: 'H',
    }
    _period_map1 = dict((v, k) for k, v in _period_map.items())

    _peering_map1 = {
        'T': TrafficType.PEERING,
        'F': TrafficType.GLOBAL,
    }

    _ttype_map = {
        TrafficType.GLOBAL: 'G',
        TrafficType.PEERING: 'P',
        TrafficType.ISP: 'I',
    }
    _ttype_map1 = dict((v, k) for k, v in _ttype_map.items())

    @classmethod
    def from_string(cls, s):
        assert(isinstance(s, str))
        l = s.strip().split(' ')
        if len(l) != 5:
            raise ValueError('Incorrect format')

        print(l)

        try:
            period = cls._period_map1[l[0]]
            if l[1] in cls._peering_map1:
                ttype = cls._peering_map1[l[1]]
            else:
                ttype = cls._ttype_map1[l[1]]
            date = summary_str2date(l[2])
            ib = int(l[3])
            ob = int(l[4])
        except (ValueError, KeyError):
            log.exception('Unable to parse backup string')
            raise
        else:
            return cls(period, ttype, date, ib, ob)

    @classmethod
    def from_summary(cls, srec):
        assert(isinstance(srec, StatsSummary))

        return cls(srec.period,
                   srec.ttype,
                   srec.sdate,
                   srec.in_bytes,
                   srec.out_bytes)

    def __init__(self, period, ttype, date, ib, ob):
        self.period = period
        self.ttype = ttype
        self.date = date
        self.ib = ib
        self.ob = ob

    def __str__(self):
        t = (self._period_map[self.period],
             self._ttype_map[self.ttype],
             summary_date2str(self.date),
             str(self.ib),
             str(self.ob))
        return ' '.join(t)


def backup_create():
    fname = settings('tasks.final.backup-file')

    with open(fname, 'w') as f:
        q = DBSession.query(StatsSummary).\
            filter(StatsSummary.final == true()).\
            order_by(StatsSummary.period).\
            order_by(StatsSummary.sdate)

        for srec in q:
            sb = SummaryBackup.from_summary(srec)
            f.write(str(sb) + '\n')


def backup_restore():
    fname = settings('tasks.final.backup-file')

    dset = DirtyDatesSet()

    with open(fname, 'r', newline='') as f:
        for line in f:
            sb = SummaryBackup.from_string(line)

            log.debug('Restoring StatsSummary for %s, date=%s, ttype=%s' % (
                sb.period.name, summary_date2str(sb.date), sb.ttype.name))
            srec = get_summary_record(sb.date, sb.period, sb.ttype, False)
            srec.final = True
            srec.accurate = True
            srec.in_bytes = sb.ib
            srec.out_bytes = sb.ob

            dset.add(sb.period.super(), sb.date)

            DBSession.add(srec)

    update_summary_dirty(dset)

    log.debug('Updating all summary...')
    update_all_summary()


@transact
def task_set_final(before_date):
    edate = date_for_period(before_date, PeriodType.HOUR)
    log.info('Make stats final before date: %s' % edate)

    log.debug('Updating all summary..')
    cascaded_summary_update(PeriodType.HOUR)

    log.debug('Setting summary final...')
    _cascaded_set_final(edate)

    log.debug('Deleting stats before %s' % summary_date2str(edate))
    DBSession.query(StatsTimestamp).filter(StatsTimestamp.ts < edate).delete()

    log.debug('Creating final summary backup...')
    backup_create()
