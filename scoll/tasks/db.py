import logging

from alembic.config import Config
from alembic import command

from scoll.settings import settings
from scoll.models import (
    DBSession,
    Base,
    )


log = logging.getLogger(__name__)


def task_init_db():
    engine = DBSession.get_bind()
    Base.metadata.create_all(engine)

    alembic_cfg = Config(settings('alembic.config'))
    command.stamp(alembic_cfg, 'head')
