import os
import re
import logging

from subprocess import (
    check_output,
    check_call,
    Popen,
    PIPE,
    CalledProcessError,
    TimeoutExpired
)

from scoll.settings import settings

log = logging.getLogger(__name__)

remote_address = None
remote_path = None


def _parse_ssh_uri(uri):
    m = re.match(r'^(?P<address>(\W+@)?[a-zA-Z0-9_-]+):(?P<path>\S+)$', uri)
    if m is None:
        raise ValueError('Failed parsing ssh url: %s' % uri)

    return m.group('address'), m.group('path')


def get_remote_files_list(fhost, fpath):
    ls_pattern = os.path.join(fpath, '*.csv')
    args = ['ssh', fhost, 'ls -1 %s' % ls_pattern]
    output = check_output(args, universal_newlines=True, timeout=20)
    full_names = filter(lambda s: len(s) > 0, output.split('\n'))
    flist = list(map(lambda p: os.path.split(p)[1], full_names))

    # Skip possibly unfinished log files (the last one)
    if len(flist):
        return flist[:-1]

    return []


def download_remote_files(flist, fhost, fpath):
    dst_dir = settings('tasks.fetch.parser-dir')
    tgz = os.path.join(dst_dir, '_dwn.tgz')

    ssh_args = ['ssh', fhost, 'tar czf - -T - -C %s .' % fpath]

    with open(tgz, 'wb') as outf:
        ssh = Popen(ssh_args, stdin=PIPE, stdout=outf)
        ssh.communicate('\n'.join(flist).encode('ascii'))

    log.debug('Unpacking archive...')
    check_call(['tar', 'zxf', tgz, '-C', dst_dir])

    log.debug('Deleting archive...')
    os.remove(tgz)


def delete_remote_files(flist, fhost, fpath):
    shell = 'cd %s; while read ff ; do rm "$ff" ; done' % fpath
    args = ['ssh', fhost, shell]

    ssh = Popen(args, stdin=PIPE)
    ssh.communicate('\n'.join(flist).encode('ascii'))


def make_backup(flist):
    src = settings('tasks.fetch.parser-dir')
    dst = settings('tasks.fetch.backup-dir')

    for f in flist:
        check_call(['cp', os.path.join(src, f), os.path.join(dst, f)])


def fetch_remote_data():
    fhost, fpath = _parse_ssh_uri(settings('tasks.fetch.remote-ssh-uri'))

    try:
        log.info('Requesting files list...')
        flist = get_remote_files_list(fhost, fpath)
        log.debug('Count of files to download: %s' % len(flist))
        log.info('Downloading files...')
        download_remote_files(flist, fhost, fpath)
        log.info('Making backup...')
        make_backup(flist)
        log.info('Deleting downloaded files on remote host...')
        delete_remote_files(flist, fhost, fpath)
    except (TimeoutExpired, CalledProcessError):
        log.exception('Unable to fetch data')
        raise


def task_fetch():
    fetch_remote_data()
