import os
import csv
from datetime import datetime


class StatsFile:
    def __init__(self, fname, ts):
        self.fname = fname
        self.ts = ts
        self._list = []

    def add_line(self, line):
        self._list.append(line)

    def __iter__(self):
        return iter(self._list)

    def __len__(self):
        return len(self._list)


class StatsLine:
    def __init__(self, names, vals):
        self._vals = dict(zip(names, vals))

    def __getitem__(self, name):
        return self._vals[name]

    def __setitem__(self, name, val):
        self._vals[name] = val


def parse_csv(fname, ts):
    with open(fname, newline='') as f:
        reader = csv.reader(f)

        try:
            names = next(reader)
        except StopIteration:
            return []

        sfile = StatsFile(fname, ts)
        for row in reader:
            sfile.add_line(StatsLine(names, row))

        return sfile


def get_timestamp(fname):
    name = os.path.split(fname)[1]
    return datetime.strptime(name, 'traffic-%Y%m%d_%H%M.csv')


def read_stats_file(fname):
    try:
        ts = get_timestamp(fname)
    except ValueError:
        return None

    stats = parse_csv(fname, ts)
    return stats
