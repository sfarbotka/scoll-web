import os
import logging
from sqlalchemy import true

from scoll.settings import settings
from scoll.tasks.importer.parser import read_stats_file
from scoll.tasks.scache import scache_update_dirty, scache_set_dirty
from scoll.tasks.summary import (
    update_summary_dirty,
    DirtyDatesSet,
    cascaded_summary_update
)
from scoll.tasks.ttype import update_ttype
from scoll.types import PeriodType
from scoll.utils.datetime import date_for_period, timestamp_date2str
from scoll.utils.db import transact
from scoll.models import (
    DBSession,
    Stats,
    StatsTimestamp,
    StatsSummary
)


log = logging.getLogger(__name__)


def _query_or_add_ts(timestamp):
    ts = DBSession.query(StatsTimestamp).filter(
        StatsTimestamp.ts == timestamp).first()

    if ts is not None:
        return None

    ts = StatsTimestamp(ts=timestamp)
    DBSession.add(ts)

    return ts


def _check_final(stats):
    sdate = date_for_period(stats.ts, PeriodType.HOUR)
    q = DBSession.query(StatsSummary).filter(
        StatsSummary.sdate == sdate,
        StatsSummary.final == true()
    )

    return q.first() is not None


def _update_summary_and_cache(trec):
    dset = DirtyDatesSet()
    dset.add(PeriodType.HOUR, trec.ts)
    update_summary_dirty(dset)

    scache_set_dirty(trec)


@transact
def add_stats(stats):
    if _check_final(stats):
        log.debug('Skipping final date: %s' % (stats.ts, ))
        return

    sts = _query_or_add_ts(stats.ts)
    if sts is None:
        log.debug('Timestamp record with date=%s is already created' % (
            timestamp_date2str(stats.ts), ))
        return

    for s in stats:
        if not s['SRC_IP'] or not s['DST_IP']:
            continue

        r = Stats()
        r.timestamp = sts
        r.src_ip = s['SRC_IP']
        r.dst_ip = s['DST_IP']
        r.src_port = s['SRC_PORT']
        r.dst_port = s['DST_PORT']
        r.num_packets = s['PACKETS']
        r.num_bytes = s['BYTES']

        update_ttype(r, s['SRC_IP'], s['DST_IP'])

        DBSession.add(r)
    _update_summary_and_cache(sts)


@transact
def _cascaded_update_summary():
    cascaded_summary_update(PeriodType.HOUR)


@transact
def _update_stats_cache():
    scache_update_dirty()


def task_import():
    parser_dir = settings('tasks.fetch.parser-dir')
    finished_dir = settings('tasks.fetch.finished-dir')

    for fname in sorted(os.listdir(parser_dir)):
        fpath = os.path.join(parser_dir, fname)
        stats = read_stats_file(fpath)
        if (stats is not None) and len(stats):
            log.debug('Adding %s (%d recs)' % (fname, len(stats)))
            add_stats(stats)
        else:
            log.debug('Skipping %s' % fname)

        os.rename(fpath, os.path.join(finished_dir, fname))

    _update_stats_cache()
    _cascaded_update_summary()
