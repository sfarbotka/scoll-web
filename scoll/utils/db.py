import logging
from functools import wraps

from transaction import manager as tmanager


log = logging.getLogger(__name__)


class RollbackException(Exception):
    pass


_force_rollback = False


def force_rollback(force=True):
    global _force_rollback
    _force_rollback = force


def transact(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            with tmanager:
                log.debug('Transaction is started')
                try:
                    f(*args, **kwargs)
                except:
                    log.debug('Transaction will rollback')
                    raise

                if _force_rollback:
                    log.info('Transaction is forced to rollback')
                    raise RollbackException

                log.debug('Transaction will commit')
        except RollbackException:
            pass

    return wrapper
