from scoll.types import TrafficType


class ParamTrafficDir:
    _tdir_keys = ('ALL', 'IN', 'OUT', 'NONE')
    _tdir_map = {
        'ALL': (True, None),
        'IN': (False, True),
        'OUT': (False, False),
        'NONE': (False, None)
    }
    _tdir_map1 = dict((v, k) for k, v in _tdir_map.items())

    @staticmethod
    def default():
        return ParamTrafficDir._tdir_map1[(True, None)]

    @staticmethod
    def all_values():
        return ParamTrafficDir._tdir_keys

    def __init__(self, sparam):
        s = sparam.upper()

        try:
            self.all, self.inbound = ParamTrafficDir._tdir_map[s]
        except KeyError:
            raise ValueError

    def __str__(self):
        return ParamTrafficDir._tdir_map1[(self.all, self.inbound)]


class ParamTrafficType:
    @staticmethod
    def default():
        return 'ALL'

    @staticmethod
    def all_values():
        a = [ParamTrafficType.default()]
        a.extend(v.name for v in TrafficType)
        return a

    def __init__(self, sparam):
        s = sparam.upper()
        if s == self.default():
            self.val = None
        else:
            try:
                self.val = TrafficType[s]
            except KeyError:
                raise ValueError

    def __str__(self):
        if self.val is None:
            return self.default()
        return self.val.name

