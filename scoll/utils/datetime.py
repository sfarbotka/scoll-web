from datetime import datetime, timedelta
from calendar import monthrange

from scoll.models import PeriodType


def date_range(start, end, step=PeriodType.HOUR, include_end=False):
    assert(isinstance(start, datetime))
    assert(isinstance(end, datetime))

    if include_end:
        end = increment_date(end, step)

    date = date_for_period(start, step)
    while date < end:
        yield date
        date = increment_date(date, step)


def datetime_range(start, end, step=timedelta(hours=1)):
    assert(isinstance(start, datetime))
    assert(isinstance(end, datetime))
    assert(isinstance(step, timedelta))

    dt = start
    while dt < end:
        yield dt
        dt += step


def increment_date(date, period):
    assert(period in PeriodType)
    assert(period is not PeriodType.ALL)

    date = date_for_period(date, period)
    y, m, d, h = date.year, date.month, date.day, date.hour

    py = (period is PeriodType.YEAR)
    pm = (period is PeriodType.MONTH)
    pd = (period is PeriodType.DAY)
    ph = (period is PeriodType.HOUR)

    if ph:
        h += 1
        if h > 23:
            h = 0
            pd = True

    if pd:
        d += 1
        if d > monthrange(y, m)[1]:
            d = 1
            pm = True

    if pm:
        m += 1
        if m > 12:
            m = 1
            py = True

    if py:
        y += 1

    return datetime(year=y, month=m, day=d, hour=h)


def date_for_period(date, period):
    assert(period in PeriodType)

    if period is PeriodType.ALL:
        return datetime(1000, 1, 1)
    if period is PeriodType.YEAR:
        return datetime(date.year, 1, 1)
    if period is PeriodType.MONTH:
        return datetime(date.year, date.month, 1)
    if period is PeriodType.DAY:
        return datetime(date.year, date.month, date.day)
    if period is PeriodType.HOUR:
        return datetime(date.year, date.month, date.day, date.hour)

    raise NotImplementedError('Unknown PeriodType value: %r' % (period, ))


summary_date_format = '%Y-%m-%d-%H'
summary_date_format_readable = 'YYYY-MM-DD-HH'


def summary_date2str(date):
    return date.strftime(summary_date_format)


def summary_str2date(date_string):
    return datetime.strptime(date_string, summary_date_format)


timestamp_date_format = '%Y-%m-%d %H:%M'


def timestamp_date2str(date):
    return date.strftime(timestamp_date_format)


def timestamp_str2date(date_string):
    return datetime.strptime(date_string, timestamp_date_format)
