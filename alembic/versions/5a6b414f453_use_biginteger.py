"""use biginteger

Revision ID: 5a6b414f453
Revises: 2ebb05fb1d3
Create Date: 2014-09-02 16:46:04.853855

"""

# revision identifiers, used by Alembic.
revision = '5a6b414f453'
down_revision = '2ebb05fb1d3'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column(
        table_name='stats_summary',
        column_name='in_bytes',
        nullable=False,
        type_=sa.BigInteger,
        existing_type=sa.Integer)
    op.alter_column(
        table_name='stats_summary',
        column_name='out_bytes',
        nullable=False,
        type_=sa.BigInteger,
        existing_type=sa.Integer)


def downgrade():
    op.alter_column(
        table_name='stats_summary',
        column_name='in_bytes',
        nullable=False,
        type_=sa.Integer,
        existing_type=sa.BigInteger)
    op.alter_column(
        table_name='stats_summary',
        column_name='out_bytes',
        nullable=False,
        type_=sa.Integer,
        existing_type=sa.BigInteger)
