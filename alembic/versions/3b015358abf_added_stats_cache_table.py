"""Added stats_cache table

Revision ID: 3b015358abf
Revises: 107765f8070
Create Date: 2014-10-25 12:05:53.409082

"""

# revision identifiers, used by Alembic.
revision = '3b015358abf'
down_revision = '107765f8070'

from alembic import op
import sqlalchemy as sa


def upgrade():
    c = sa.CheckConstraint('ttype IN (0, 1, 2, 3, 100)')
    op.create_table('stats_cache',
    sa.Column('cache_id', sa.Integer(), nullable=False),
    sa.Column('ts_id', sa.Integer(), nullable=False),
    sa.Column('in_bytes', sa.BigInteger(), nullable=False),
    sa.Column('out_bytes', sa.BigInteger(), nullable=False),
    sa.Column('unk_bytes', sa.BigInteger(), nullable=False),
    sa.Column('accurate', sa.Boolean(), nullable=False),
    sa.Column('ttype', sa.SmallInteger(), c, nullable=False),
    sa.ForeignKeyConstraint(['ts_id'], ['stats_ts.ts_id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('traffic_id')
    )


def downgrade():
    op.drop_table('stats_cache')
