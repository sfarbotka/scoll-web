"""summary peering to ttype

Revision ID: 99f7988d0d
Revises: 33b5a61465a
Create Date: 2014-09-10 16:57:04.365741

"""

# revision identifiers, used by Alembic.
revision = '99f7988d0d'
down_revision = '33b5a61465a'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    c = sa.CheckConstraint('ttype IN (0, 1, 2)')
    op.add_column('stats_summary', sa.Column('ttype', sa.SmallInteger(),
                                             c, nullable=False))
    op.drop_column('stats_summary', 'peering')


def downgrade():
    op.add_column('stats_summary', sa.Column('peering', 
                                             mysql.TINYINT(display_width=1),
                                             autoincrement=False,
                                             nullable=False))
    op.drop_column('stats_summary', 'ttype')
