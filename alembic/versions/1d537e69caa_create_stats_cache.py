"""create stats_cache

Revision ID: 1d537e69caa
Revises: None
Create Date: 2014-08-28 11:07:37.399162

"""

# revision identifiers, used by Alembic.
revision = '1d537e69caa'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('stats_cache',
    sa.Column('cache_id', sa.Integer(), nullable=False),
    sa.Column('stats_date', sa.DateTime(), nullable=False),
    sa.Column('period', sa.Integer(), nullable=False),
    sa.Column('in_bytes', sa.Integer(), nullable=False),
    sa.Column('out_bytes', sa.Integer(), nullable=False),
    sa.Column('accurate', sa.Boolean(), nullable=False),
    sa.PrimaryKeyConstraint('cache_id'),
    sa.CheckConstraint('period in (1, 2, 3)', name='check_period'),
    sa.Index('idx_date_period', 'stats_date', 'period', unique=True)
    )


def downgrade():
    op.drop_table('stats_cache')
