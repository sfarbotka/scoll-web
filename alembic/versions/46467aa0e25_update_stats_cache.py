"""update stats_cache

Revision ID: 46467aa0e25
Revises: 1d537e69caa
Create Date: 2014-08-28 17:05:41.654520

"""

# revision identifiers, used by Alembic.
revision = '46467aa0e25'
down_revision = '1d537e69caa'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('stats_cache', sa.Column('final', sa.Boolean(), nullable=False))
    op.add_column('stats_cache', sa.Column('peering', sa.Boolean(), nullable=False))


def downgrade():
    op.drop_column('stats_cache', 'peering')
    op.drop_column('stats_cache', 'final')
