"""stats_alter

Revision ID: 33b5a61465a
Revises: 5a6b414f453
Create Date: 2014-09-10 14:21:46.690074

"""

# revision identifiers, used by Alembic.
revision = '33b5a61465a'
down_revision = '5a6b414f453'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('stats', sa.Column('is_inbound', sa.Boolean(), nullable=True))
    op.add_column('stats', sa.Column('ttype', sa.SmallInteger(), nullable=False))
    op.drop_column('stats', 'is_peering')
    op.drop_column('stats', 'is_src_local')
    op.drop_column('stats', 'is_dst_local')
    op.drop_column('stats', 'comment')
    op.create_index(op.f('ix_stats_ttype'), 'stats', ['ttype'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_stats_ttype'), table_name='stats')
    op.add_column('stats', sa.Column('comment', mysql.TEXT(), nullable=True))
    op.add_column('stats', sa.Column('is_dst_local',
                                     mysql.TINYINT(display_width=1),
                                     autoincrement=False, nullable=True))
    op.add_column('stats', sa.Column('is_src_local',
                                     mysql.TINYINT(display_width=1),
                                     autoincrement=False, nullable=True))
    op.add_column('stats', sa.Column('is_peering',
                                     mysql.TINYINT(display_width=1),
                                     autoincrement=False, nullable=True))
    op.drop_column('stats', 'ttype')
    op.drop_column('stats', 'is_inbound')
