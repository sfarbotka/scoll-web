"""timestamp index

Revision ID: 107765f8070
Revises: 99f7988d0d
Create Date: 2014-10-24 17:42:37.258485

"""

# revision identifiers, used by Alembic.
revision = '107765f8070'
down_revision = '99f7988d0d'

from alembic import op


def upgrade():
    op.create_index(op.f('ix_stats_ts_ts'), 'stats_ts', ['ts'], unique=True)


def downgrade():
    op.drop_index(op.f('ix_stats_ts_ts'), table_name='stats_ts')
