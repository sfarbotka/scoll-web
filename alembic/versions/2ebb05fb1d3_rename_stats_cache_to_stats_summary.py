"""rename stats_cache to stats_summary

Revision ID: 2ebb05fb1d3
Revises: 46467aa0e25
Create Date: 2014-08-28 18:35:10.824565

"""

# revision identifiers, used by Alembic.
revision = '2ebb05fb1d3'
down_revision = '46467aa0e25'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    op.create_table('stats_summary',
    sa.Column('summary_id', sa.Integer(), nullable=False),
    sa.Column('sdate', sa.DateTime(), nullable=False),
    sa.Column('period', sa.Integer(), nullable=False),
    sa.Column('peering', sa.Boolean(), nullable=False),
    sa.Column('in_bytes', sa.Integer(), nullable=False),
    sa.Column('out_bytes', sa.Integer(), nullable=False),
    sa.Column('accurate', sa.Boolean(), nullable=False),
    sa.Column('final', sa.Boolean(), nullable=False),
    sa.PrimaryKeyConstraint('summary_id')
    )
    op.drop_table('stats_cache')


def downgrade():
    op.create_table('stats_cache',
    sa.Column('cache_id', mysql.INTEGER(display_width=11), nullable=False),
    sa.Column('stats_date', mysql.DATETIME(), nullable=False),
    sa.Column('period', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False),
    sa.Column('in_bytes', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False),
    sa.Column('out_bytes', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False),
    sa.Column('accurate', mysql.TINYINT(display_width=1), autoincrement=False, nullable=False),
    sa.Column('final', mysql.TINYINT(display_width=1), autoincrement=False, nullable=False),
    sa.Column('peering', mysql.TINYINT(display_width=1), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('cache_id'),
    )
    op.drop_table('stats_summary')
