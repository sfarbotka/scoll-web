__requires__ = 'scoll'
import sys
from pkg_resources import load_entry_point

sys.exit(
   load_entry_point('scoll', 'console_scripts', 'scoll-tasks')()
)
