__requires__ = 'pyramid'
import sys
from pkg_resources import load_entry_point

sys.exit(
   load_entry_point('pyramid', 'console_scripts', 'pserve')()
)
