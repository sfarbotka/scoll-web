import os

from setuptools import setup


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'SQLAlchemy',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'GeoIP',
    'alembic',
    'PyMySQL',
    ]

packages = ['scoll']

package_data = {
    'scoll': ['templates', 'static']
}

entry_points = """\
[paste.app_factory]
main = scoll:main

[console_scripts]
scoll-tasks = scoll.scripts.tasks:main
"""

setup(name='scoll',
      version='0.1',
      description='Network traffic statistics collector',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=packages,
      package_data=package_data,
      include_package_data=True,
      zip_safe=False,
      test_suite='scoll',
      install_requires=requires,
      entry_points=entry_points,
      )
